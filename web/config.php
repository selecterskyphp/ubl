<?php
/**
 * 
 * 数据库配置文件
 *
 */
 return array(
	'DB_DSN'=>'sqlite:./db.sqlite',
	'DB_PREFIX'=>'tbl_',
	'DB_TYPE'=>'pdo',
	'DB_CHARSET' => 'utf8', 
	'DB_FIELDS_CACHE' => false,
);
?>
