BEGIN TRANSACTION;
CREATE TABLE "tbl_vm" ("id" INTEGER PRIMARY KEY  NOT NULL ,"server" VARCHAR(32) NOT NULL  DEFAULT (null) ,"username" VARCHAR(32) NOT NULL ,"password" VARCHAR(32) NOT NULL );
CREATE TABLE "tbl_device" ("id" INTEGER PRIMARY KEY  NOT NULL ,"vmid" INTEGER NOT NULL ,"name" VARCHAR(128) NOT NULL ,"code" VARCHAR(128) NOT NULL ,"status" INTEGER NOT NULL  DEFAULT (0) , "deviceid" VARCHAR(128), "stream_id" VARCHAR(128), "uk" VARCHAR(128), "shareid" VARCHAR(128), "errorMessage" VARCHAR(255), "password" VARCHAR(32), "keepliveTime" INTEGER, "online" BOOL DEFAULT 0);
CREATE TABLE "tbl_token" ("token" VARCHAR(128) NOT NULL  DEFAULT (null) ,"deviceToken" VARCHAR(128) NOT NULL  DEFAULT (null) ,"updatetime" INTEGER DEFAULT (null) ,"uname" VARCHAR(20));
COMMIT;
