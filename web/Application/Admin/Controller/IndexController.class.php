<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;



class IndexController extends Base {

    function __construct()
    {
        parent::__construct();        
        
        $this->dao = M('Device');
    }

    public function index()
    {
        $list = $this->dao->select();
        $mod = M('Vm');
        for($i=0;$i<count($list);$i++)
        {
            $row = $mod->getById($list[$i]['vmid']);
            $list[$i]['vmname'] = $row['server'].'('.$row['username'].')';
        }
        $this->assign('list',$list);
        $this->assign('HOME_ON',' class="active"');
        $this->assign('TITLE','首页'.$this->title); 

	    $this->display();
    }
    public  function edit()
    {
        $this->assign('HOME_ON',' class="active"');
        $this->assign('TITLE','增加设备'.$this->title); 
        $mod = M('Vm');
        $list = $mod->select();
        $this->assign('vmlist',$list);
        $vo = array('vmid'=>I('vmid',-1,'intval'),'keyword'=>I('keyword'));    
          
        if($vo['vmid']>=0)
        {
            $row = $mod->getById($vo['vmid']);
            if(!$row)
            {
                $this->error('VM服务器不存在');
            }
            $result = $this->ubl->query($row['server'], $row['username'], $row['password'], $vo['keyword']);
            if($result['code'] != 0)
            {
                $this->error('连接VM服务器失败，错误码：'.$result['code'].',错误信息：'.$result['message']);
            }

            $list = $result['data'];
            $mod = M('Device');

            for($i=0; $i<count($list);$i++)
            {
                $where = array('vmid'=>$vo['vmid'],'code'=>$list[$i]['code']);
                $row = $mod->where($where)->find();
                if(is_array($row))
                {
                    $list[$i]['isadd'] = 1;
                }
                else
                {
                    $list[$i]['isadd'] = 0;
                }
            }
            $codes = array();
            $onlines = array();
            $isadds = array();
            foreach ($list as $key=>$value)
            {
                $codes[$key] = $value['code'];
                $onlines[$key] = $value['online'];
                $isadds[$key] = $value['isadd'];
            }
            array_multisort($isadds,SORT_NUMERIC,SORT_ASC,$onlines,SORT_NUMERIC,SORT_DESC,$codes,SORT_STRING,SORT_DESC,$list);
            $this->assign('list',$list);

        }
        //var_dump($vo,$list);
        $this->assign('vo',$vo);
        $this->display();
    }

    public function addDevice()
    {
        $vmid = I('vmid',-1,'intval');
        $name = I('name');
        $code = I('code');
        $online = I('online',0,'intval');
        $keyword = I('keyword');
        if($vmid==-1 || !$name || !$code)
        {
            $this->error('参数错误');
        }
        $result = $this->wowcam->addDevice($vmid, $name, $code, $online);
        if($result['code'] != 0)
        {
            $this->error($result['message']);
        }
        else
        {
            $this->ubl->notify();
            $this->assign('jumpUrl',U('Index/edit',array('vmid'=>$vmid,'keyword'=>$keyword)));
            $this->success('操作成功');
        }

    }

    public function status()
    {
        $id = I('id',0,'intval');
        $value = I('value',-1,'intval');

        if($value == -1 || !$id)
        {
            $this->error("参数错误");
        }
        $data = array('status'=>$value);
        $mod = M('Device');
        $where = array('id'=>$id);
        $result = $mod->data($data)->where($where)->save();
        if($result)
        {
            $this->ubl->notify();
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('操作成功');
        }
        else
        {
            $this->error('操作失败');
        }
    }

    public function del()
    {
        $id = I('id');
        $result = $this->wowcam->delDevice($id);

        if($result['code'] != 0)
        {
            $this->error($result['message']);
        }
        else
        {
            $this->ubl->notify();
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('操作成功');
            //$this->redirect(U('Index/index'));
        }
        
    }

    public function modify()
    {
        $this->assign('MODIFY_ON',' class="active"');
        $this->assign('TITLE','修改密码'.$this->title); 
        $this->display();
    }

    public function modifysave()
    {
        $old = I('oldpwd');
        $pwd1 = I('pwd1');
        $pwd2 = I('pwd2');

        if(!$old || !$pwd1)
        {
            $this->error('请输入正确的密码');
        }

        if($pwd1 != $pwd2)
        {
            $this->error('两次密码输入不一样');
        }

        $result = $this->wowcam->modify(md5($old),md5($pwd1),md5($pwd2));

        if($result['code'] == 0)
        {
            $this->assign('jumpUrl',U('Index/logout'));
            $this->success('密码修改成功，请重新登录');
        }
        else
        {
            $this->error($result['message']);
        }
    }



    public  function login()
    {
        $this->display();
    }

    public function doLogin()
    {
        $username = I('post.name');
        $pwd = I('post.pwd');
        if(!$username || !$pwd)
        {
            $this->error('请输入正确登录信息');
        }
        $result = $this->wowcam->login($username,md5($pwd));
        if($result['code'] != 0)
        {
            //var_dump($result);
            $this->error($result['message']);
        }
        
        $this->assign('jumpUrl',U('Index/index'));
        $this->success('登录成功！');
    }

    public function logout()
    {
        $this->wowcam->logout();
        $mod = M('Token');
        $data = array('updatetime'=>0);
        $where = array();
        $where['token'] = array('neq','');
        $result = $mod->where($where)->data($data)->save();
        
        $this->redirect('Index/login');
    }
}
