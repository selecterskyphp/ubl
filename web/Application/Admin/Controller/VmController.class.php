<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;



class VmController extends Base {

    function __construct()
    {
        parent::__construct();        

        $this->dao = M('Vm');
        $this->assign('VM_ON',' class="active"');
    }

    public function index()
    {
        $list = $this->dao->select();
        $this->assign('list',$list);
        $this->assign('TITLE','VM服务器'.$this->title); 

	    $this->display();
    }

    public function save()
    {   
        $server = I('server');
        $password = I('password');
        $username = I('username');
        if(!$server || !$username)
        {
            $this->error('请填写必要的参数');
        }

        $id = I('id',0,'intval');     
        $mod = M('Vm');   
        //修改且没有设置则认为不对密码进行操作
        if($id>0 && !$password)
        {
            unset($_POST['password']);   
            $row = $mod->getById($id);
            if(!$row)
            {
                $this->error('修改的记录不存在');
            }
            $password = $row['password'];
        }
        //其它情况，使用md5加密
        else
        {
            $_POST['password'] = md5($_POST['password']);
            $password = $_POST['password'];
            //增加记录 检查一下记录是否已经存在了
            if($id == 0)
            {
                
                $where = array('server'=>$server,'username'=>$username);
                $row = $mod->where($where)->find();
                if(is_array($row))
                {
                    $this->error('记录已经存在');
                }
            }
        }
        $result = $this->ubl->query($server,$username,$password,'test_connect_vmserver');

        if($result['code'] != 0)
        {
            $this->error('连接VM服务器失败，请确认');
        }
        parent::save();
    }

}