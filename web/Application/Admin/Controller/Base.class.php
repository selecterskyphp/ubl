<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Admin\Controller;


use Common\Controller\CommonBase;
use \Org\Uni\Wowcam;
use \Org\Uni\Ubl;

//3分钟检查一次token是否过期
define('TOKEN_INVAILD_TIME', 60*3);

class Base extends CommonBase
{

    protected $isLogin;
    protected $uname;
    protected $token;
    protected $title;
    protected $order;
    protected $wowcam;
    protected $ubl;

    function __construct()
    {
        parent::__construct();        
        
        $this->ubl = new Ubl();
        $this->wowcam = new Wowcam();

        $notchecklogin = C('NOT_CHECK_LOGIN_ACTION');
        if(!in_array(ACTION_NAME,$notchecklogin))
        {

            $mod = M('token');
            $data = $mod->find();
            if(!$data)
            {
                $this->redirect('Index/login');                
            }
            $this->token = $data['token'];
            $this->uname = $data['uname'];
            $updatetime = $data['updatetime'];
            $this->wowcam->setToken($this->token);

            //每3分钟到服务器检查一下超时
            if(time()-$updatetime>TOKEN_INVAILD_TIME)
            {
                
                $result = $this->wowcam->chkLogin();
                if($result['code'] != 0)
                {
                    $this->redirect('Index/login');
                }
            }     
            $this->isLogin = true;       
        }

        $this->order = 'id desc';
        $this->title =  ' - 管理后台 - '.$this->WEB_NAME;
        $this->assign('username',$this->uname);
        $this->assign('token',$this->token);
    }


    public function index()
    {
        $list = $this->dao->order($this->order)->select();
        $this->assign('list',$list);
        $this->display();
    }


    public  function edit()
    {
        $id = I('get.id',0,'intval');
        if($id>0)
        {   
            $vo = $this->dao->getById($id);           
        }
        
        $this->assign('vo',$vo);
        $this->assign('id',$id);
        $this->display();
    }

    public function save()
    {   
        // if(!isset($_POST['id']))
        // {
           
        //     $_POST['addtime'] =time();
        //     $_POST['hits'] = 0;
        //     $_POST['listorder'] = 0;
        // }


        if(!$this->dao->create($_POST))
        {
            $this->error($this->dao->getError());
        }
        $id = I('id',0,'intval');        
        if($id>0)
        {
            $result =   $this->dao->save();
        }
        else
        {
            $result =   $this->dao->add();
        }

        if(false !== $result) 
        {         
            $this->assign('jumpUrl',U(CONTROLLER_NAME.'/index'));
            $this->success('操作成功');
        }else{
            
            $this->error('操作失败');
        }
    }

    public function del()
    {
       $id = I('id',0,'intval');
       $where = array('id'=>$id);
       $this->dao->where($where)->delete();
       $this->redirect(CONTROLLER_NAME.'/index');
    }

    public function status()
    {
        $id = I('id');
        $status = I('value',0,'intval');
        if($status == 0)
        {
            $status = 1;
        }
        else if($status == 1)
        {
            $status = 0;
        }


        $data = array('status'=>$status);
        $where = array('id'=>$id);
        $result = $this->dao->data($data)->where($where)->save();
        $this->redirect(CONTROLLER_NAME.'/index');
    }


} 
