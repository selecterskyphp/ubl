<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;

use Home\Controller;

class IndexController extends Base {

    private $device;
    function __construct()
    {
        parent::__construct();
        $this->dao = M('Device');
        $where = array('status'=>1);
        $list = $this->dao->where($where)->select();
        $mod = M('Vm');
        for($i=0;$i<count($list);$i++)
        {   
            $row = $mod->getById($list[$i]['vmid']);
            $list[$i]['vmserver'] = $row['server'];
            $list[$i]['vmusername'] = $row['username'];
        }
        $this->assign('list',$list);
    }
    public function test()
    {
        $this->display();
    }

    public function index(){
        //$this->redirect('Index/add', array('cate_id' => 2), 5, '页面跳转中...');
        
        $this->assign('title','我的摄像机');
	    $this->display();
    }

    public function play()
    {
        $id = I('id',0,'intval');
        $mod = M('Device');
        $row = $mod->getById($id);
        if(!$row)
        {
            $this->error('记录不存在');
        }

        $result = $this->wowcam->play($row['shareid'],$row['uk']);
        if($result['error_code'] != 0)
        {
            $this->error($result['error_msg'].',code:'.$result['error_code']);
        }
        // var_dump($result);
        // exit();
        $this->assign('url',$result['data']['url']);
        $this->assign('title',$row['name'].' - 实况');
        $this->assign('playid',$id);
        $this->display();


    }

}
