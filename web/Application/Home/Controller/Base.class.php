<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Home\Controller;


use Common\Controller\CommonBase;

use \Org\Uni\Wowcam;

class Base extends CommonBase
{
	protected $wowcam;
    function __construct()
    {
        parent::__construct();
        $this->wowcam = new Wowcam();

    }

} 
