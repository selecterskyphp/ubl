<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-6-4
 * Time: 下午5:13
 */

namespace Api\Controller;
use \Org\Uni\Wowcam;

class UblController extends Base
{

    //
    public function query()
    {
        $mod = M('Device');
        $list = $mod->field('vmid,name,code,status,deviceid,stream_id,uk,shareid')->select();
        $mod = M('Token');
        $token = $mod->select();
        if(!$token)
        {
            $this->out(null,5);
        }
        $mod = M('Vm');
        for($i=0;$i<count($list);$i++)
        {
            $row = $mod->getById($list[$i]['vmid']);
            $list[$i]['vmserver']=$row['server'];
            $list[$i]['vmusername']=$row['username'];
            $list[$i]['vmpassword']=$row['password'];
        }
        
        $data = array('token'=>$token[0]['deviceToken'],'list'=>$list);
        $this->out($data);
    }

    public function listDevice()
    {
        $mod = M('Device');
        $list = $mod->field('id,name,code,status,deviceid')->select();
        $this->out($list);
    }

    public function play()
    {
        $id = I('id','0','intval');
        if(!$id)
        {
            $this->out(null,1);
        }
        $mod = M('Device');
        $row = $mod->getById($id);
        if(!is_array($row))
        {
            $this->out(null,5);
        }
        $wowcam = new Wowcam();
        $result = $wowcam->play($row['shareid'],$row['uk']);

        if($result['error_code'] != 0)
        {
            $this->out(null,$result['error_code'],$result['error_code']);
        }
        else
        {
            $this->out($result['data']['url']);
        }
    }

    public function keepLive()
    {
        $deviceid = I('deviceid');
        $mod = M('Device');
        $where = array('deviceid'=>$deviceid);
        $data = array('keepliveTime'=>time());
        $re = $mod->where($where)->data($data)->save();
        if(!$re)
        {
            $this->out(null, 8);
        }
        $this->out();
    }

    public function status()
    {
        $deviceid = I('deviceid');
        $online = I('online',0,'intval');
        $msg = I('message');
        $where = array('deviceid'=>$deviceid);
        $data = array('online'=>$online,'errorMessage'=>$msg);
        $mod = M('Device');
        $re = $mod->where($where)->data($data)->save();
        if(!$re)
        {
            $this->out(null, 8);
        }
        $this->out();
    }


} 
