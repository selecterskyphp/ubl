<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Api\Controller;


use Common\Controller\CommonBase;

/**
 * API控制块总的基础类
 */
class Base extends CommonBase
{

    function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $method = I('method');
        //
        if(method_exists($this,$method))
        {
            $this->$method();
        }
        else
        {
            $this->out('index',1);
        }

    }
    protected function out($data=array(),$code=0,$msg='',$type=0)
    {
        $result = array();
        if($code != 0)
        {
            $result = errorByCode($code,$msg,$type);
            $result['data'] = $data;
        }
        else
        {
            $result = successByData($data);
        }
        $this->outResult($result);
    }

    protected function outResult($result)
    {
        header("Content-Type: text/json");
        if($result['code'] != 0)
        {
            header("HTTP/1.0 400 Bad Request");
        }
        //支持jsonp
        $callback = I('callback');
        $ecode = json_encode($result);
        if($callback)
        {
            echo $callback.'('.$ecode.')';
        }
        else
        {
            echo $ecode;
        }
        
        exit(0);
    }

} 