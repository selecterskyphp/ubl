<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-30
 * Time: 下午2:18
 */

namespace Org\Uni;

use \Org\Uni;

class Wowcam extends UniBase{

    function __construct()
    {
        $this->api= 'http://localhost/api/1.0/ubl';       
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function chkLogin()
    {
        if(!$this->token)
        {
            return errorByCode(3);
        }
        $param = array('method'=>'chkLogin');
        $result = $this->sendRequest($param);
        return $result;
    }

    public function login($username, $password)
    {
        $param = array('method'=>'login','username'=>$username,'password'=>$password);
        $result = $this->sendRequest($param,'',false);

        //登陆成功后，则要更新本地的token
        if($result['code'] == 0)
        {
            $this->token = $result['data']['token'];            
            $mod = M('Token');
            //数据里面只会存在一条记录
            //$where = array('uname'=>$username);
            $row = $mod->find();
            $data = array('token'=>$this->token,'deviceToken'=>$result['data']['deviceToken'],'updatetime'=>time());
            $re = false;
            //记录已经存在
            
            if($row)
            {
                $where = array();
                $where['token'] = array('neq','');
                $re = $mod->data($data)->where($where)->save();
            }
            else
            {
                $data['uname'] = $username;
                $re = $mod->data($data)->add();
            }
            if(!$re)
            {
                //var_dump($mod->getLastSql());exit();
                return errorByCode(2);
            }

        }
        return $result;
    }

    public function logout()
    {
        $param = array('method'=>'logout');
        $result = $this->sendRequest($param);
        return $result;
    }

    public function modify($oldpwd,$pwd1,$pwd2)
    {
        $param = array('method'=>'modify','oldpwd'=>$oldpwd,'pwd1'=>$pwd1,'pwd2'=>$pwd2);
        $result = $this->sendRequest($param);
        return $result;
    }

    public function addDevice($vmid,$name,$code,$online)
    {
        $mod = M('Device');
        $where = array('vmid'=>$vmid,'code'=>$code);
        $row = $mod->where($where)->find();
        if($row)
        {
            $result = errorByCode(4);
            return $result;
        }
        $param = array('method'=>'addDevice', 'desc'=>$name);
        $result = $this->sendRequest($param);

        //增加设备成功了 要存入本地数据库
        if($result['code'] == 0)
        {
            $resultData = $result['data'];
            $data = array(
                'vmid'=>$vmid,
                'name'=>$name,
                'code'=>$code,
                'status'=>0,
                'deviceid'=>$resultData['deviceid'],
                'stream_id'=>$resultData['stream_id'],
                'uk'=>$resultData['uk'],
                'shareid'=>$resultData['shareid'],
                'password'=>$resultData['password'],
                'online'=>$online,
                'keepliveTime'=>0,
                'errorMessage'=>''
                );

            $re = $mod->data($data)->add();
            //添加到数据失败了（这个情况很少出现，除非是代码问题或参数问题，这里出现了需要及时修改代码）
            if(!$re)
            {
                //尝试删除设备
                $param = array('method'=>'delDevice','deviceid'=>$deviceid);
                $this->sendRequest($param);                
                return errorByCode(6);
            }            
        }
        return $result;
    }

    public function delDevice($id)
    {
        $mod = M('Device');

        $where = array('id'=>$id);
        $row = $mod->where($where)->find();
        if(!$row)
        {
            return errorByCode(5);
        }
        $deviceid = $row['deviceid'];
        $param = array('method'=>'delDevice','deviceid'=>$deviceid);
        $result = $this->sendRequest($param);
        //var_dump($result);
        //操作成功了，则删除本地数据库的设备
        //if($result['code'] == 0)
        //{
            $re = $mod->where($where)->delete();
            if(!$re)
            {
                return errorByCode(7);
            }
            $result = successByData(array());
        //}
        return $result;
    }

    public function play($shareid,$uk)
    {
        $url = "https://pcs.baidu.com/rest/2.0/pcs/device";
        $param = array('method'=>'liveplay','shareid'=>$shareid,'uk'=>$uk);
        $result = $this->sendPlayRequest($param, $url);        
        return $result;
    }
    private function sendPlayRequest($param,$url = '',$token=true,$post=false)
    {
        $errorType = C('JSON_ERROR_TYPE');

        if($token)
        {
            $param['token'] = $this->token;
        }
        if(!$url)
        {
            $url = $this->api;
        }
        $result = array();
        if($post === true)
        {
            $result = do_post($url,$param);
        }
        else
        {
            $result = do_get($url,$param);
        }
         // var_dump($url,$param,$result);
         // exit();

        if($result['code'] !== 0)
        {
            $data = errorByCode($result['code'],$result['message'],$errorType['WOWCAM']);
            return $data;
        }
        $resultData = $result['data'];
        

        if($result['http_code'] != 200 || $resultData['code'] != 0)
        {
            $data = errorByCode($resultData['code'],$resultData['message'],$errorType['WOWCAM']);                   
            return $data;
        }
      
        $data = successByData($resultData);
        return $data;
    }
    

}
