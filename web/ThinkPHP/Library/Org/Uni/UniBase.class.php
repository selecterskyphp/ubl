<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-30
 * Time: 下午2:18
 */

namespace Org\Uni;

use \Org\Uni;

class UniBase {

    protected $api;
    protected $token;

    protected function sendRequest($param,$url = '',$token=true,$post=false)
    {
        $errorType = C('JSON_ERROR_TYPE');

        if($token)
        {
            $param['token'] = $this->token;
        }
        if(!$url)
        {
            $url = $this->api;
        }
        $result = array();
        if($post === true)
        {
            $result = do_post($url,$param);
        }
        else
        {
            $result = do_get($url,$param);
        }
         // var_dump($url,$param,$result);
         // exit();

        if($result['code'] !== 0)
        {
            $data = errorByCode($result['code'],$result['message'],$errorType['WOWCAM']);
            return $data;
        }
        $resultData = $result['data'];
        //var_dump($resultData);exit();

        if($result['http_code'] != 200 || $resultData['code'] != 0)
        {
            $data = errorByCode($resultData['code'],$resultData['message'],$errorType['WOWCAM']);                   
            return $data;
        }
      
        $data = successByData($resultData['data']);
        return $data;
    }

}
