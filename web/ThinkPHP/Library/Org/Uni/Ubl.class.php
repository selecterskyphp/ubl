<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-30
 * Time: 下午2:18
 */

namespace Org\Uni;

use \Org\Uni;


class Ubl extends UniBase{

    private $baseApi;
    function __construct()
    {
        $this->baseApi = 'http://127.0.0.1:10080/api/1/';
    }
    public function query($server,$username,$password,$keyword)
    {
        $this->api = $this->baseApi.'camera';
        $param = array('server'=>$server,'username'=>$username,'password'=>$password,'keyword'=>$keyword);
        $result = $this->sendRequest($param, null, false);
        return $result;
    }

    public function notify()
    {
        $this->api = $this->baseApi.'notify';
        $param = array();
        $result = $this->sendRequest($param, null, false);
        return $result;
    }

}
