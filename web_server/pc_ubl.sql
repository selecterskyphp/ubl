-- phpMyAdmin SQL Dump
-- version 4.0.10.2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014-10-21 14:19:07
-- 服务器版本: 5.1.73
-- PHP 版本: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `pc_ubl`
--

-- --------------------------------------------------------

--
-- 表的结构 `tbl_admin`
--

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `pwd` varchar(32) NOT NULL,
  `logintime` int(10) unsigned NOT NULL,
  `ip` varchar(32) NOT NULL,
  `loginNum` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `uid`, `name`, `pwd`, `logintime`, `ip`, `loginNum`) VALUES
(1, 2756661489, 'ubl', '1d9a4119ce806c15da8dcb8c3a65665f', 0, '', 0),
(3, 3795036516, 'dongyuan', 'e10adc3949ba59abbe56e057f20f883e', 0, '', 0),
(5, 1090618718, 'cjx', 'e10adc3949ba59abbe56e057f20f883e', 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_device`
--

DROP TABLE IF EXISTS `tbl_device`;
CREATE TABLE IF NOT EXISTS `tbl_device` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `deviceid` varchar(255) NOT NULL,
  `key` varchar(128) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `streamid` varchar(128) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `share` tinyint(3) unsigned NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `expiretime` int(11) DEFAULT NULL,
  `cvrday` int(11) DEFAULT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `romVer` varchar(20) NOT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `pushAlarm` tinyint(3) unsigned NOT NULL,
  `pushEmail` tinyint(3) unsigned DEFAULT '0',
  `emailSmtpServer` varchar(120) DEFAULT NULL,
  `emailSmtpUsername` varchar(120) DEFAULT NULL,
  `emailSmtpPassword` varchar(120) DEFAULT NULL,
  `emailReceive` varchar(120) DEFAULT NULL,
  `emailSmtpPort` int(11) DEFAULT '23',
  `emailSend` varchar(120) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `tbl_device`
--

INSERT INTO `tbl_device` (`id`, `uid`, `deviceid`, `key`, `desc`, `streamid`, `status`, `share`, `thumbnail`, `expiretime`, `cvrday`, `addtime`, `romVer`, `ip`, `pushAlarm`, `pushEmail`, `emailSmtpServer`, `emailSmtpUsername`, `emailSmtpPassword`, `emailReceive`, `emailSmtpPort`, `emailSend`) VALUES
(22, 2756661489, 'ubl_GONAWD', 'uHWoFz', '长隆野生动物世界--熊猫餐厅', 'e3ed33d0214b11e4aa0900259089e31a', 23, 1, 'http://d.pcs.baidu.com/file/c32ea2c1399ee1536145f7f30bcc5127?fid=2756661489-1537715-515723475626789&time=1410774725&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-nCgO6vWqnJVgCh0VyqJKp66fV3o%3D&expires=1410781925&rt=sh&owner=1537715', 1415462400, 7, 1408104777, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(21, 2756661489, 'ubl_HfRqYD', 'UIZkMd', '长隆野生动物世界-熊猫中心', '55fa31381d1111e4815aac853dd1c904', 23, 1, 'http://d.pcs.baidu.com/file/75e3dbc9a88fbb91d639e3009567b677?fid=2756661489-1537715-515195104010979&time=1410774725&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-qqj5vW86LZwm2LBsQjlxB7HO8%2FA%3D&expires=1410781925&rt=sh&owner=1537715', 1415030400, 7, 1408104777, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(19, 2756661489, 'ubl_GIXhWa', 'nNLiBD', '长隆野生动物世界-熊猫中心2', 'a12089101d1011e4aa0900259089e31a', 0, 0, '', 1415030400, 7, 1408104765, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(20, 2756661489, 'ubl_DbXnOe', 'HEhIsM', '长隆野生动物世界-熊猫中心2', '4a400ca01d1111e4815aac853dd1c904', 23, 1, 'http://d.pcs.baidu.com/file/95e6ea10ccd8a0881fa9621b27e07da1?fid=2756661489-1537715-1007511916269427&time=1410774725&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-WLCqDMtevzG3G1EmVOragNKtqZQ%3D&expires=1410781925&rt=sh&owner=1537715', 1415030400, 7, 1408104777, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(5, 2135190584, 'UV01', 'BDYIMy', '无线款型 IPC', 'dcbba82ea99311e39f58ac853dd1c8c0', 0, 1, 'http://d.pcs.baidu.com/file/ce97e0709da35fc21c10a856baad7fc1?fid=2135190584-1537715-622807264173039&time=1408329549&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-fjcQO9HG0kXMtSkqiiLdHX36G8Q%3D&expires=1408336749&rt=sh&owner=1537715', 1401897600, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(6, 2135190584, 'UV02', 'NMeKZv', '55', '89c7c9f6fda611e3815aac853dd1c904', 0, 0, 'http://d.pcs.baidu.com/file/044c5d0b7b085fff0666ec4a5d9a2cda?fid=2135190584-1537715-658583719377117&time=1408329549&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-%2B15QfF3YLc%2F9YFIOn2GKNHSvNWE%3D&expires=1408336749&rt=sh&owner=1537715', 1411574400, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(7, 2135190584, '22', 'twlnxa', 'dd', '8609a1740c8111e4aa0900259089e31a', 0, 1, '', 1413129600, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(8, 2135190584, 'UV00', 'iqVbTG', '基本', '0b27187c1c4611e4aa0900259089e31a', 0, 0, 'http://d.pcs.baidu.com/file/0dccfef4fc327814eabffc5efee21586?fid=2135190584-1537715-422951027072118&time=1409564607&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-dQ%2BrjahGswU8NIGb%2BsfhGgrQSoM%3D&expires=1409571807&rt=sh&owner=1537715', 1401897600, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(9, 2135190584, 'ubl_nztCsK', 'XadrIR', 'ubl_nztCsK', '0fbd564c1de211e4815aac853dd1c904', 0, 1, 'http://d.pcs.baidu.com/file/b2270e5b1f58d74ffdf4d7cb53aaaca9?fid=2135190584-1537715-558018509922296&time=1408329549&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-g0WZFvSUM2B7tOCScVtTr0kICnk%3D&expires=1408336749&rt=sh&owner=1537715', 1411920000, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(10, 2135190584, 'dd', 'FSteQY', 'dxx', 'a2e2cddc22e011e4aa0900259089e31a', 0, 0, '', 1415635200, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(11, 2135190584, 'uipc', 'rstAYa', 'uipc', '2d607b1822e511e4815aac853dd1c904', 0, 0, '', 1415030400, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(12, 2135190584, 'ddf', 'IWOxsV', 'dd', 'ec4a4a62244e11e4aa0900259089e31a', 0, 0, '', 1415808000, 7, 1408102989, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(13, 1090618718, 'ubl_AjKJXT', 'cxsZjC', 'sss', '50b70872247311e4815aac853dd1c904', 0, 1, '', 1415808000, 7, 1408103842, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(23, 2756661489, 'test', 'fRZgWO', 'estste', '1a706f32247411e4aa0900259089e31a', 0, 0, '', 1415808000, 7, 1408105249, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(24, 0, '210235T05P0123123123', 'ToYkOy', 'uipc007', 'aa814ed226d811e4aa0900259089e31a', 0, 1, 'http://d.pcs.baidu.com/file/03f67b2b705bb2c9bb0ae989e3eab179?fid=2135190584-1537715-376965671879611&time=1408502895&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-ENzGOCMW%2BHS0W6BYDlGH1FbZaac%3D&expires=1408510095&rt=sh&owner=1537715', 1416067200, 7, 1408502575, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(25, 0, 'ubl', 'BPCZhi', 'ubl', 'a0273aae276f11e4815aac853dd1c904', 0, 0, '', 1416153600, 7, 1408502575, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(26, 0, '210235c0l03142000007', 'vBWhJq', 'uipc-2', '26653f5e279211e4815aac853dd1c904', 0, 1, 'http://d.pcs.baidu.com/file/c218b3c7713d2e717d5b5076d1fdb085?fid=2135190584-1537715-639828451835406&time=1408502895&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-1SlMSStM0hmgQtcF%2FiDBxxGzyU8%3D&expires=1408510095&rt=sh&owner=1537715', 1416153600, 7, 1408502575, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(27, 2135190584, '210235C0L03142000001', 'nHtxTl', 'uipc001', 'c2904654287211e4aa0900259089e31a', 0, 0, '', 1416240000, 7, 1409591538, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(28, 2135190584, '210235C0L03142000004', 'hXtQgI', 'uipc004', 'e8798de0288f11e4815aac853dd1c904', 0, 0, '', 1416326400, 7, 1409591538, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(29, 2135190584, 'sffjjjjh7643332222', 'MfqLCN', 'SSS', 'e0de19c431a011e4aa0900259089e31a', 0, 0, '', 1417276800, 7, 1409591538, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(30, 2135190584, 'sffjjjjh7643332222a', 'pwMIRa', 'SSS', 'e673a2e631a011e4aa0900259089e31a', 0, 0, '', 1417276800, 7, 1409591538, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_device_bak`
--

DROP TABLE IF EXISTS `tbl_device_bak`;
CREATE TABLE IF NOT EXISTS `tbl_device_bak` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `deviceid` varchar(255) NOT NULL,
  `key` varchar(128) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `streamid` varchar(128) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `share` tinyint(3) unsigned NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `expiretime` int(11) DEFAULT NULL,
  `cvrday` int(11) DEFAULT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `romVer` varchar(20) NOT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `pushAlarm` tinyint(3) unsigned NOT NULL,
  `pushEmail` tinyint(3) unsigned DEFAULT '0',
  `emailSmtpServer` varchar(120) DEFAULT NULL,
  `emailSmtpUsername` varchar(120) DEFAULT NULL,
  `emailSmtpPassword` varchar(120) DEFAULT NULL,
  `emailReceive` varchar(120) DEFAULT NULL,
  `emailSmtpPort` int(11) DEFAULT '23',
  `emailSend` varchar(120) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- 转存表中的数据 `tbl_device_bak`
--

INSERT INTO `tbl_device_bak` (`id`, `uid`, `deviceid`, `key`, `desc`, `streamid`, `status`, `share`, `thumbnail`, `expiretime`, `cvrday`, `addtime`, `romVer`, `ip`, `pushAlarm`, `pushEmail`, `emailSmtpServer`, `emailSmtpUsername`, `emailSmtpPassword`, `emailReceive`, `emailSmtpPort`, `emailSend`) VALUES
(33, 2147483647, 'ubl_siGSMq', 'ZtJDgA', '001-迎宾路入口_26', '4d06a842064411e4815aac853dd1c904', 23, 1, 'http://d.pcs.baidu.com/file/746e599e62a4e819fb1371cfa602426b?fid=2756661489-1537715-251344751473502&time=1405577251&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-lZoTR6YQq7fzDugKnLO94rl5xLg%3D&expires=1405584451&rt=sh&owner=1537715', 1412524800, 7, 1405577251, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(2, 2135190584, 'UV01', 'TkfFLr', '无线款型 IPC', 'dcbba82ea99311e39f58ac853dd1c8c0', 0, 1, 'http://d.pcs.baidu.com/file/ce97e0709da35fc21c10a856baad7fc1?fid=2135190584-1537715-622807264173039&time=1408102562&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-y%2Frm%2Bm3EKpVoUSWoOGcwT0tM%2Fo4%3D&expires=1408109762&rt=sh&owner=1537715', 1401897600, 7, 1403751867, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(3, 2135190584, 'UV02', 'amwGYT', '55', '89c7c9f6fda611e3815aac853dd1c904', 0, 0, 'http://d.pcs.baidu.com/file/044c5d0b7b085fff0666ec4a5d9a2cda?fid=2135190584-1537715-658583719377117&time=1408102562&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-AVGpJCTLxI%2BiIdjlrrDiVQdaV34%3D&expires=1408109762&rt=sh&owner=1537715', 1411574400, 7, 1403837748, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(55, 2135190584, 'ubl_nztCsK', 'VeYATB', 'ubl_nztCsK', '0fbd564c1de211e4815aac853dd1c904', 23, 1, 'http://d.pcs.baidu.com/file/8b326d3a1b4e8c5dcd725a506d586a85?fid=2135190584-1537715-605513515890453&time=1408102562&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-eX%2BRcYOv3Wjx88BiAI7zsob9%2BqM%3D&expires=1408109762&rt=sh&owner=1537715', 1411920000, 7, 1407381749, '1.0', '192.168.100.135', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(56, 0, 'ubl_IDctOl', 'ULRauq', '060-迎宾大道入口-27', '8ca8f8d816c511e4aa0900259089e31a', 23, 1, 'http://d.pcs.baidu.com/file/b13f96d6f548e63b10bde6812221f85e?fid=2756661489-1537715-997304066826351&time=1407396973&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-oiUh9YNUes556G2IXK3R1fVe8eA%3D&expires=1407404173&rt=sh&owner=1537715', 1414339200, 7, 1407396446, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(18, 2147483647, 'ubl_FWrfJQ', 'CnUyMY', 'ab1', '774515d0000411e4815aac853dd1c904', 0, 1, '', 1411833600, 7, 1404097999, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(19, 2147483647, 'ubl_SWqfyp', 'KmDiBY', 'oaa01', 'accf9f36000411e4815aac853dd1c904', 0, 1, '', 1411833600, 7, 1404098088, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(20, 2147483647, 'ubl_pwlVzJ', 'uipAto', 'oaa01', '7237940e00eb11e4815aac853dd1c904', 0, 1, '', 1411920000, 7, 1404197208, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(22, 2147483647, 'ubl_OlXDwx', 'iXLVmq', '001-迎宾路入口_26', 'ce4a09bc025711e4815aac853dd1c904', 0, 1, '', 1412092800, 7, 1404353950, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(23, 2147483647, 'ubl_wEpsHT', 'eScDBu', '011迎宾大道汉溪入口', '390dc830026011e49f58ac853dd1c8c0', 23, 1, 'http://d.pcs.baidu.com/file/b0e8578f2440ab0463eb5fad5464bd39?fid=2756661489-1537715-29095105006956&time=1404357621&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-RDIhGKDGaOLcPcC3DDFoGwd7tFo%3D&expires=1404364821&rt=sh&owner=1537715', 1412092800, 7, 1404357622, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(30, 2135190584, '22', 'ivrNpE', 'dd', '8609a1740c8111e4aa0900259089e31a', 0, 1, '', 1413129600, 7, 1405471193, '1.0', '', 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(31, 2135190584, 'aaa', 'xvHTdE', 'sss', 'b7c558ac0c8111e4aa0900259089e31a', 0, 0, '', 1413216000, 7, 1405471201, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(35, 2135190584, 'UV00', 'wLruSx', '基本', '0b27187c1c4611e4aa0900259089e31a', 5, 0, 'http://d.pcs.baidu.com/file/0dccfef4fc327814eabffc5efee21586?fid=2135190584-1537715-422951027072118&time=1408102562&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-JP9bhPABHey9qjHkqGcJVtt0I2w%3D&expires=1408109762&rt=sh&owner=1537715', 1401897600, 7, 1407204790, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(36, 2135190584, 'hhh', 'nFKlLs', 'sss', 'b585a8701d4411e4815aac853dd1c904', 0, 0, '', 1415030400, 7, 1407314279, '1.0', '', 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(57, 0, 'ubl_IxCqiG', 'chHAgk', 'XJ0718W马园小龙马展区', '2268617c196111e4815aac853dd1c904', 23, 0, 'http://d.pcs.baidu.com/file/50fb68eeec00111abce31a8ea43bddf0?fid=2756661489-1537715-499274229193343&time=1407396973&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-Wx%2BrVIpDWY9g%2BJdigypTUmALjuQ%3D&expires=1407404173&rt=sh&owner=1537715', 1414598400, 7, 1407396446, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(58, 0, 'ubl_GIXhWa', 'zGoBZW', '长隆野生动物世界-熊猫中心2', 'a12089101d1011e4aa0900259089e31a', 0, 0, '', 1415030400, 7, 1407396446, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(59, 0, 'ubl_DbXnOe', 'SuYGco', '长隆野生动物世界-熊猫中心2', '4a400ca01d1111e4815aac853dd1c904', 23, 1, 'http://d.pcs.baidu.com/file/11d9050f26069327be1b9f55b9f8cb9e?fid=2756661489-1537715-26129098060548&time=1407838242&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-ZC4vJ7dtSAU1sbPxCDeBo4DVXQQ%3D&expires=1407845442&rt=sh&owner=1537715', 1415030400, 7, 1407396446, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(60, 0, 'ubl_HfRqYD', 'PcUHQh', '长隆野生动物世界-熊猫中心', '55fa31381d1111e4815aac853dd1c904', 23, 1, 'http://d.pcs.baidu.com/file/1f1cf43176c30357b615e5d6cce55e58?fid=2756661489-1537715-912484722567321&time=1407838242&sign=FDTAERO-DCb740ccc5511e5e8fedcff06b081203-torwZt9fhvxvN1m6%2FWGb0%2Fu9%2Fno%3D&expires=1407845442&rt=sh&owner=1537715', 1415030400, 7, 1407396446, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(61, 2135190584, 'YFRF36WH140721010', 'KANShV', 'UIPC', '162b69221e2f11e4aa0900259089e31a', 0, 0, '', 1415116800, 7, 1407414831, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(62, 2147483647, 'ubl_ixOVpq', 'xwFMdq', '长隆野生动物世界--熊猫餐厅', '4d758782213b11e4815aac853dd1c904', 1, 0, NULL, NULL, NULL, 1407749931, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(63, 2147483647, 'ubl_GONAWD', 'VAFUyx', '长隆野生动物世界--熊猫餐厅', 'e3ed33d0214b11e4aa0900259089e31a', 1, 0, NULL, NULL, NULL, 1407757056, '1.0', NULL, 0, 0, NULL, NULL, NULL, NULL, 23, NULL),
(69, 2147483647, 'ubl_xeSRkY', 'urtixm', 'oaa01', '5fda0b2a246d11e4815aac853dd1c904', 1, 0, NULL, NULL, NULL, 1408101290, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(65, 2135190584, 'dd', 'qtKiFl', 'dxx', 'a2e2cddc22e011e4aa0900259089e31a', 0, 0, '', 1415635200, 7, 1407932361, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(66, 2135190584, 'uipc', 'fgCJqE', 'uipc', '2d607b1822e511e4815aac853dd1c904', 0, 0, '', 1415030400, 7, 1407932843, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(67, 2147483647, 'ubl_BRjidq', 'Qwditm', 'oaa01', '3cedfcbe246511e4aa0900259089e31a', 1, 0, NULL, NULL, NULL, 1408097796, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(68, 2135190584, 'ddf', 'GEJeDM', 'dd', 'ec4a4a62244e11e4aa0900259089e31a', 0, 0, '', 1415808000, 7, 1408099286, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(71, 2147483647, 'ubl_SXoBek', 'kaAVnT', 'sss', '17de0c88247011e4815aac853dd1c904', 1, 0, NULL, NULL, NULL, 1408102458, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL),
(72, 2147483647, 'ubl_zFsnZE', 'YTxbwo', 'sss', '955a9654247011e4815aac853dd1c904', 1, 0, NULL, NULL, NULL, 1408102669, '1.0', NULL, 1, 0, NULL, NULL, NULL, NULL, 23, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_device_version`
--

DROP TABLE IF EXISTS `tbl_device_version`;
CREATE TABLE IF NOT EXISTS `tbl_device_version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deviceid` varchar(128) NOT NULL,
  `version` varchar(20) NOT NULL,
  `minVersion` varchar(20) NOT NULL,
  `addtime` int(11) NOT NULL,
  `desc` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_fav`
--

DROP TABLE IF EXISTS `tbl_fav`;
CREATE TABLE IF NOT EXISTS `tbl_fav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `shareid` varchar(128) DEFAULT NULL,
  `uk` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `tbl_fav`
--

INSERT INTO `tbl_fav` (`id`, `uid`, `shareid`, `uk`) VALUES
(8, 0, 'fd335349db3dab75bd5e62dc2ce3522a', '2756661489'),
(7, 0, 'ba21823e8e0438cee8eda9ef0ddd0431', '2135190584'),
(9, 0, '192f54d25b2e03436f59175266ad7ed9', '2756661489'),
(11, 2135190584, 'ba21823e8e0438cee8eda9ef0ddd0431', '2135190584'),
(12, 0, 'bf21ce96f5666dff53241fc7fd274f6e', '2756661489'),
(13, 0, '4803bd101b11e3937cecf4a3abc9547f', '2756661489'),
(14, 0, '7429c1e40794f2c87a23fd12020790bc', '2135190584'),
(15, 0, 'da54ef24a62bdba51d8ee0b025143983', '3795036516'),
(16, 0, 'a3e17aa460a717bf63d7d85efcea630f', '2756661489'),
(17, 0, 'a9c6f39858c036f36c6e4587794045f0', '2135190584');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_feedback`
--

DROP TABLE IF EXISTS `tbl_feedback`;
CREATE TABLE IF NOT EXISTS `tbl_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `desc` varchar(300) NOT NULL,
  `replay` varchar(150) DEFAULT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `replaytime` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `tbl_feedback`
--

INSERT INTO `tbl_feedback` (`id`, `uid`, `desc`, `replay`, `addtime`, `replaytime`) VALUES
(1, 2135190584, '界面效果有效改进，易用性有待改进，愿越做越好', NULL, 0, NULL),
(2, 2135190584, '界面效果有效改进，易用性有待改进，愿越做越好', NULL, 1403242954, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_help`
--

DROP TABLE IF EXISTS `tbl_help`;
CREATE TABLE IF NOT EXISTS `tbl_help` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `tbl_help`
--

INSERT INTO `tbl_help` (`id`, `title`, `desc`, `addtime`, `hits`, `listorder`, `status`) VALUES
(2, 'test', 'ststststst', 1403242954, 0, 0, 1),
(3, '1212', '1212', 1403243084, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_history`
--

DROP TABLE IF EXISTS `tbl_history`;
CREATE TABLE IF NOT EXISTS `tbl_history` (
  `clientType` tinyint(3) unsigned NOT NULL,
  `clientVersion` int(10) unsigned NOT NULL,
  `clientOs` varchar(128) NOT NULL,
  `ime` varchar(128) NOT NULL,
  `updatetime` int(10) unsigned NOT NULL,
  `lang` tinyint(3) unsigned NOT NULL,
  `token` varchar(128) NOT NULL,
  `total` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_token`
--

DROP TABLE IF EXISTS `tbl_token`;
CREATE TABLE IF NOT EXISTS `tbl_token` (
  `token` varchar(128) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `addtime` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_token`
--

INSERT INTO `tbl_token` (`token`, `uid`, `addtime`) VALUES
('2756661489-5763c80582b26df87abd2f2a9afe36d5', 2756661489, 1413363950);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `uid` int(10) unsigned NOT NULL,
  `uname` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `refresh_token` varchar(128) NOT NULL,
  `updatetime` int(10) unsigned NOT NULL,
  `portrait` varchar(128) NOT NULL,
  `pushAlarm` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `token` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_user`
--

INSERT INTO `tbl_user` (`uid`, `uname`, `token`, `refresh_token`, `updatetime`, `portrait`, `pushAlarm`) VALUES
(2756661489, 'ub...1@163.com', '21.0077d33ccb147d1b5d22a8bcda4d27a1.2592000.1415955950.2756661489-1537715', '22.b03b99aba40d2747c9bb3e80d0b55fc5.315360000.1728723950.2756661489-1537715', 1413363950, '1ea42e4b', 0),
(3795036516, 'donyun66', '21.34ac596e820ec3547a636cc04763fb68.2592000.1410693938.3795036516-1537715', '22.dc4893da68aa3a42b566b546245a4ba8.315360000.1723461938.3795036516-1537715', 1408101938, 'e7e2646f6e79756e3636bb37', 0),
(1849562994, 'bcam888', '23.e61bb87a284ace61ab8cf0c84b9ff92f.2592000.1409394020.1849562994-1537715', '', 1407726035, 'http://tb.himg.baidu.com/sys/portraitn/item/596e6263616d383838ad3a', 0),
(1476409825, 'wangboxy2008', '21.959bc961bb17831bda2b3602eda780d3.2592000.1409842789.1476409825-1537715', '22.123b6eb964e146aa6ba579316b2c5ad9.315360000.1722610789.1476409825-1537715', 1407250789, '775877616e67626f7879323030383e04', 0),
(1090618718, 'selectersky', '21.e014fb0baf7432fae30dd79e3c935b23.2592000.1410695695.1090618718-1537715', '', 1408103695, 'http://tb.himg.baidu.com/sys/portraitn/item/cb4173656c6563746572736b798105', 1),
(2320889085, 'seeyou', '23.5c84e717c9cb807b6a2cc7d932451d66.2592000.1411999865.2320889085-1537715', '', 1410256356, 'http://tb.himg.baidu.com/sys/portraitn/item/ba8a2251', 1),
(2596945, 'soarhh', '23.08a32506ba21fb11bb044580736b4d9a.2592000.1413094723.2596945-1537715', '', 1412990960, 'http://tb.himg.baidu.com/sys/portraitn/item/ee00736f617268688e23', 1),
(3527710093, 'HappyDuk', '23.dd58fd82242cad34c3f77fe2054564e6.2592000.1413767380.3527710093-1537715', '', 1411175383, 'http://tb.himg.baidu.com/sys/portraitn/item/dbd2486170707944756b5240', 1),
(1460617910, '茶树菇113', '23.38a8cb2255936cf8b9b4802b4260ca60.2592000.1413820434.1460617910-1537715', '', 1413611478, 'http://tb.himg.baidu.com/sys/portraitn/item/0c57e88cb6e6a091e88f87313133690b', 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_version`
--

DROP TABLE IF EXISTS `tbl_version`;
CREATE TABLE IF NOT EXISTS `tbl_version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `minVersion` int(11) NOT NULL,
  `addtime` int(11) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `tbl_version`
--

INSERT INTO `tbl_version` (`id`, `code`, `name`, `minVersion`, `addtime`, `desc`, `url`, `type`, `filesize`) VALUES
(1, 1, '1.0', 1, 1, 'android初步版本', '1', 1, 0),
(2, 1, '1.0', 1, 1, 'ios初步版本', '2', 2, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
