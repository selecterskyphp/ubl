<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Admin\Controller;


use Common\Controller\CommonBase;

class Base extends CommonBase
{

    protected $isLogin;
    protected $username;
    protected $uid;
    protected $title;
    protected $order;

    function __construct()
    {
        parent::__construct();

        $this->username = I('session.adminusername');
        $this->uid = I('session.adminuid');

        if($this->username && $this->uid)
        {
            $this->isLogin = true;
        }
        else
        {
            $this->isLogin = false;
        }

        $notchecklogin = C('NOT_CHECK_LOGIN_ACTION');
        if(!in_array(ACTION_NAME,$notchecklogin))
        {
            if(!$this->isLogin)
            {
//                var_dump($_SESSION,$this->uid,$this->token,$this->username);
//                exit;
                $this->redirect('Index/login');
            }
        }
        $this->order = 'id desc';
        $this->title =  ' - 管理后台 - '.$this->WEB_NAME;
        $this->assign('username',$this->username);
        $this->assign('uid',$this->uid);
    }

    protected function setLoginStatus($status)
    {
        if($status)
        {
            $_SESSION['adminuid'] = $this->uid;
            $_SESSION['adminusername'] = $this->username;

            $this->isLogin = true;
        }
        else
        {
            $_SESSION['adminuid'] = '';
            $_SESSION['adminusername'] = '';

            $this->uid = '';
            $this->username = '';
            $this->isLogin = false;
        }
    }

    public function index()
    {
        $list = $this->dao->order($this->order)->select();
        $this->assign('list',$list);
        $this->display();
    }


    public  function edit()
    {
        $id = I('get.id',0,'intval');
        $vo = array('status'=>1,'listorder'=>0);

        if($id>0)
        {   
            $vo = $this->dao->getById($id);           
        }
        
        $this->assign('vo',$vo);
        $this->display();
    }

    public function save()
    {   
        // if(!isset($_POST['id']))
        // {
           
        //     $_POST['addtime'] =time();
        //     $_POST['hits'] = 0;
        //     $_POST['listorder'] = 0;
        // }


        if(!$this->dao->create($_POST))
        {
            $this->error($this->dao->getError());
        }
        if(intval($_POST['id'])>0)
        {
            $result =   $this->dao->save();
        }
        else
        {
            $result =   $this->dao->add();
        }

        if(false !== $result) 
        {         
            $this->assign('jumpUrl',U(CONTROLLER_NAME.'/index'));
            $this->success('操作成功');
        }else{
            
            $this->error('操作失败');
        }
    }

    public function del()
    {
       $id = I('id');
       $this->dao->delete($id);
       $this->redirect(CONTROLLER_NAME.'/index');
    }

    public function status()
    {
        $id = I('id');
        $status = I('value',0,'intval');
        if($status == 0)
        {
            $status = 1;
        }
        else if($status == 1)
        {
            $status = 0;
        }


        $data = array('status'=>$status);
        $where = array('id'=>$id);
        $result = $this->dao->data($data)->where($where)->save();
        $this->redirect(CONTROLLER_NAME.'/index');
    }


} 