<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;


class FeedbackController extends Base {

    function __construct()
    {
        parent::__construct();
        $this->dao = M('Feedback');
        $this->assign('FEEDBACK_ON',' class="active"');
        $this->assign('TITLE','意见反馈'.$this->title);

    }

    public function index()
    {
        $list = $this->dao->order('id desc')->select();
        $mod = M('User');
        for($i=0;$i<count($list);$i++)
        {
            $row = $mod->getByUid($list[$i]['uid']);
            $list[$i]['uname'] = $row['uname'];
        }
        $this->assign('list',$list);
        $this->display();
    }
}