<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;


class HelpController extends Base {

    function __construct()
    {
        parent::__construct();
        $this->dao = M('Help');
        $this->assign('HELP_ON',' class="active"');
        $this->assign('TITLE','帮助'.$this->title);

    }
    public function index()
    {
        $this->order = 'listorder desc,id desc';
        parent::index();
    }
    public function save()
    {
        $id = I('id',0,'intval');
        if(!$id)
        {
           
            $_POST['addtime'] =time();
            $_POST['hits'] = 0;
            $_POST['listorder'] = 0;
            $_POST['status'] = 1;
        } 
        parent::save();
    }

}