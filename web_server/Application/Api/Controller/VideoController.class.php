<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-5-6
 * Time: 上午9:22
 */

namespace Api\Controller;


class VideoController extends AppBase{

    private $deviceid;
    function _initialize()
    {
        //$this->dao = M('Devices')
        $this->deviceid = I('deviceid');

        $not_check_deviceid_action_array = array('startShareLive');
        $method = I('method');

        if( !(in_array(ACTION_NAME, $not_check_deviceid_action_array)||in_array($method, $not_check_deviceid_action_array))  && !$this->deviceid)
        {
            $this->out(null,1);
        }
    }

    public function queryReplay()
    {
        $st = I('st',0,'int');
        $et = I('et',0,'int');

        if(!$st || !$et || $st > $et)
        {
            $this->out(null,1);
        }

        $result = $this->device->queryReplay($this->deviceid, $st, $et);
        $this->outResult($result);
    }

    public function startLive()
    {
        $result = $this->device->startLive($this->deviceid);
        $this->outResult($result);
    }
    public function startShareLive()
    {
        $shareid = I('shareid');
        $uk = I('uk');
        $result = $this->device->startLiveByShare($shareid, $uk);
        $this->outResult($result);
    }
    public function stopLive()
    {

    }
    public function startReplay()
    {
        $st = I('st',0,'int');
        $et = I('et',0,'int');
        $result = $this->device->startReplay($this->deviceid, $st, $et);
        $this->outResult($result);
    }
    public function lastThumbail()
    {
        $result = $this->device->queryLastThumbail($this->deviceid);
        $this->outResult($result);
    }

} 
