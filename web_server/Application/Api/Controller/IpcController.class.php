<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-6-4
 * Time: 下午5:13
 */

namespace Api\Controller;
use \Org\Uni\BaiduApi;

class IpcController extends Base
{
    private $md5;
    private $data;
    private $deviceid;
    private $time;

    function __construct()
    {
        parent::__construct();

        $this->md5 = I('md5');
        $this->deviceid = I('deviceid');
        $this->time = I('time');        
        $this->dao = M('Device');

        $this->data = $this->dao->getByDeviceid($this->deviceid);
        if(!$this->data)
        {
            $this->out(null, 5);
        }
        if($this->md5 != '123456')
        {
            $md5 = md5($this->time.'-'.$this->deviceid.'-'.$this->data['key']);
            if($this->md5 != $md5)
            {
                $this->out(null, 11);
            }
        }

    }

    public function info()
    {
        $ip = I('ip');
        if($ip)
        {
            $data = array();
            $data['ip']=$ip;
            $where =array('deviceid'=>$this->deviceid);
            $this->dao->where($where)->save($data);
        }
        $mod = M('User');
        $row = $mod->getByUid($this->data['uid']);
        $this->data['token'] = $row['token'];
        $this->out($this->data);
    }


    //推送告警
    public function push($deviceid=null,$data = null)
    {
        //Vendor('BaiduPush.Channel');
        import('Vendor.BaiduPush.Channel');
        $baidu = new BaiduApi();
        $API_KEY = $baidu->getAK();
        $API_SRCET = $baidu->getSK();

        //$this->load->library('Channel',array('' => , ););
        if($deviceid)
        {
            $this->data = $this->dao->getByDeviceid($deviceid);
            if(!$this->data)
            {
                $this->out(null, 5);
            }
        }
        if(!$data)
        {
            $data = I('data','','htmlspecialchars_decode');
        }
        file_put_contents('1.txt',"\n".$data."\n",FILE_APPEND);
        $uid = $this->data['uid'];
        $dataarry = json_decode($data,true);
        //var_dump($data,$username,$dataarry);
        $channel = new \Channel ($API_KEY, $API_SRCET );
        //$r= $channel->queryBindList($uid);
        //var_dump($r);exit();
        //$channel->setHost(\Channel::HOST_IOS_DEV); 经测试 这个选项可以不需要
        //推送消息到某个user，设置push_type = 1;
        //推送消息到一个tag中的全部user，设置push_type = 2;
        //推送消息到该app中的全部user，设置push_type = 3;
        $push_type = 1;
        $optional[\Channel::USER_ID] = $uid; //如果推送单播消息，需要指定user

        //指定发到ios设备 不指定表示推送到所有用户 IOS设备必须指定类型
        $optional[\Channel::DEVICE_TYPE] = 4;
        //指定消息类型为通知
        $optional[\Channel::MESSAGE_TYPE] = 1;
        //如果ios应用当前部署状态为开发状态，指定DEPLOY_STATUS为1，默认是生产状态，值为2.
        //旧版本曾采用不同的域名区分部署状态，仍然支持。
        $optional[\Channel::DEPLOY_STATUS] = 1;
        //$optional[\Channel::TAG_NAME] = 'tag'.$uid;
        //通知类型的内容必须按指定内容发送，示例如下：
        $message = array(
            'title' => $dataarry['alarmSrcName'],
            'description'=>$dataarry['alarmDescription'],
            "open_type"=>0,
            "net_support" => 1,
            "user_confirm"=> 0,
            // "pkg_content":"",
            // "pkg_name" : "com.baidu.bccsclient",
            // "pkg_version":"0.1",
            'type'=>$dataarry['alarmType'],
            'level'=>$dataarry['alarmLevel'],
            'code'=>$dataarry['alarmSrcCode']

        );
        $senddata = json_encode($message);
        $message_key = md5($data);
        $ret = $channel->pushMessage ( $push_type, $senddata, $message_key, $optional ) ;


        if ( false === $ret )
        {

            $this->out(null, $channel->errno(),$channel->errmsg());
        }
        else
        {
            $this->out();
        }

    }

    public function doTest()
    {
        $deviceid = 'UV00';
        $data = array(
            'alarmEventCode'=>'alarmEventCode',
            'alarmSrcCode'=>'alarmSrcCode',
            'alarmSrcName'=>'alarmSrcName',
            'activeName'=>'activeName',
            'alarmType'=>503,
            'alarmLevel'=>1,
            'alarmTime'=>time('Y-m-d H:i:s'),
            'alarmDescription'=>'alarmDescription',
            'playCameraCode'=>'playCameraCode'
        );
        $this->push($deviceid,json_encode($data));
    }

} 
