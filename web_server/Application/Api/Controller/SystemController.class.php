<?php

namespace Api\Controller;
use \Org\Uni\BaiduApi;

class SystemController extends AppBase
{

    function _initialize()
    {
        $this->dao = M('User');
    }

    public function info()
    {
        if(!$this->token)
        {
            $this->out(null,3);
        }
        $result = $this->device->infoByToken();
        $this->outResult($result);        
    }

    public function pushAlarm()
    {
        $uid = I('uid',0,'intval');
        $push = I('push',0,'intval');

        $where =array('uid'=>$uid);
        $data = array('pushAlarm'=>$push);
        $re = $this->dao->where($where)->data($data)->save();
        $this->out($data);
    }

    public function helplist()
    {
        $start = I('start',0,'intval');
        $end = I('num',-1,'intval');
        $mod = M('Help');
        $where= array();        
        $where['status']=1;
        $count = $mod->where($where)->count();
        if(-1===$end)
        {
            $end=$count;
        }
        else if(0 === $end)
        {
            $end = 20;
        }
        $list = $mod->field('id,title,addtime,hits')->where($where)->order('listorder desc,id desc')->limit($start.','.$end)->select();
        $data = array('count'=>$count,'list'=>$list);
        $this->out($data);
    }

    public function helpdetail()
    {
        $id = I('id',0,'intval');
        $mod = M('Help');
        $data = $mod->getById($id);
        $this->out($data);
    }

    public function feedbacklist()
    {
        $start = I('start',0,'intval');
        $end = I('num',-1,'intval');
        $mod = M('Feedback');
        $where= array();        
        $where['uid']=$this->uid;
        $count = $mod->where($where)->count();
        if(-1===$end)
        {
            $end=$count;
        }
        else if(0 === $end)
        {
            $end = 20;
        }
        $list = $mod->field('id,desc,replay,addtime,replaytime')->where($where)->order('id desc')->limit($start.','.$end)->select();
        $data = array('count'=>$count,'list'=>$list);
        $this->out($data);
    }

    public function feedbacksave()
    {
        $msg = I('msg');
        $min = 5;
        $max = 300;
        $len = strlen($msg);
        $result = array();
        if($len<$min || $len>$max)
        {
            $result = errorByCode(12);
            $result['message'] = sprintf($result['msssage'],$min,$max);
            $this->outResult($result);
        }
        $mod = M('Feedback');
        $data = array('uid'=>$this->uid,'desc'=>$msg,'addtime'=>time());
        $result = $mod->data($data)->add();
        if($result)
        {
            $this->out();
        }
        else
        {
            $this->out(null,2);
        }
    }


}
