<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-6-4
 * Time: 下午5:13
 */

namespace Api\Controller;

use \Org\Uni\Token;
use \Org\Uni\Device;
use \Org\Uni\BaiduApi;

//设置默认的用户ID
//define('UBL_DEFAULT_UID', 2135190584);

class UblController extends Base
{
    private $token;
    private $tokenMange;
    private $device;
    private $uid;

    function __construct()
    {
        parent::__construct();        
        $this->token = I('token');
        $this->tokenMange = new Token();
        $not_check_token_action_array = array('login','doLogin');
        $method = I('method');
        if(!in_array(ACTION_NAME, $not_check_token_action_array)&&!in_array($method, $not_check_token_action_array))
        {
            if(!$this->token)
            {
                $this->out(null,3);
            }
            //验证wowcam token有效性
            $this->uid = $this->tokenMange->verify($this->token);
            if(!$this->uid)
            {
                $this->out(null,3);
            }
            //从数据库读取百度token 建立百度对象，开始和百度通信
            $mod = M('User');            
            $row = $mod->getByUid($this->uid);
            $token = $row['token'];
            $this->device = new Device($this->uid,$token);      
        }
         
    }

    //获取百度的token
    private function refreshBaiduToken($uid)
    {
        $mod = M('User');
        $row = $mod->getByUid($uid);
        $token = $row['token'];
        
        $api = new BaiduApi();

        $url = 'https://openapi.baidu.com/oauth/2.0/token';
        $para = array('grant_type'=>'refresh_token',
            'refresh_token'=>$row['refresh_token'],
            'client_id'=>$api->getAK(),
            'client_secret'=>$api->getSK()
            );
        $result = do_get($url,$para);
      // var_dump($result,$param,$row);
        if($result['code'] == 0 && $result['http_code'] == 200)
        {
            $resultData = $result['data'];
            $refresh_token = $resultData['refresh_token'];
            $token = $resultData['access_token'];

            //更新最新的token和refresh_token到数据库
            $data = array('token'=>$token,'refresh_token'=>$refresh_token, 'updatetime'=>time());
            $where = array('uid'=>$uid);
            $mod->where($where)->data($data)->save();
        }
        return $token;

    }

    //检查是否已经登录
    public function chkLogin()
    {
        $this->out();
    }

    public function login()
    {
        $username = I('username');
        $password = I('password');
        if(!$username || !$password)
        {
            $this->out(null,1);
        }
        $mod = M('Admin');
        $where = array('name'=>$username,'pwd'=>$password);
        $row = $mod->where($where)->find();
        if(!$row)
        {
            $this->out(null,14);
        }
        $this->uid = $row['uid'];
        $token = $this->tokenMange->createToken($this->uid);
        if(!$token)
        {
            $this->out(null,15);
        }      
        //刷新百度token防止过期
        $baiduToken = $this->refreshBaiduToken($this->uid);
        $data = array('token'=>$token,'deviceToken'=>$baiduToken,'uname'=>$username,'uid'=>$this->uid);
        $this->out($data);
    }

    public function logout()
    {
        $this->tokenMange->delToken($this->token);
        $this->out();
    }

    public function modify()
    {
        $oldpwd = I('oldpwd');
        $pwd1 = I('pwd1');
        $pwd2 = I('pwd2');

        if(!$oldpwd || !$pwd1 || strlen($pwd1) != 32)
        {
            $this->out(null, 1);
        }
        if($pwd1 != $pwd2)
        {
            $this->out(null, 16);
        }
        $mod = M('Admin');
        $where = array('uid'=>$this->uid,'pwd'=>$oldpwd);
        $row = $mod->where($where)->find();
        if(!$row)
        {
            $this->out(null, 14);
        }
        $data = array('pwd'=>$pwd1);
        $re = $mod->data($data)->where($where)->save();
        if($re)
        {
            $this->out();
        }
        else
        {
            $this->out(null, 2);
        }

    }

    //增加设备 desc建议为设备的名字
    public function addDevice()
    {
        $desc = I('desc');
        if(!$desc)
        {
            $this->out(null,1);
        }
        //自动生成一个deviceid
        $deviceid = 'ubl_'.rand_string();
        $mod = M('Device');
        $row = $mod->getByDeviceid($deviceid);
        //检查deviceid是否重复
        while(is_array($row))
        {
            $deviceid = 'ubl_'.rand_string();
            $row = $mod->getByDeviceid($deviceid);
        }       
        
        
        $result = $this->device->add($deviceid,$desc);
        if($result['code'] != 0)
        {
            $this->outResult($result);
        }

        $res = $this->device->share($deviceid);
        if($res['code'] != 0)
        {
            $this->outResult($res);
        }
        $result['data'] = array_merge($result['data'],$res['data']);
        $this->outResult($result);
    }

    public function delDevice()
    {
        $deviceid = I('deviceid');
        $result = $this->device->del($deviceid,$desc);
        $this->outResult($result);
    }

    public function listDevice()
    {
        $result = $this->device->query();
        $this->outResult($result);
    }

} 
