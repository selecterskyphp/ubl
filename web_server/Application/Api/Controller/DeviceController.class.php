<?php

namespace Api\Controller;

class DeviceController extends AppBase
{
    private $deviceid;

    function _initialize()
    {
        //$this->dao = M('Devices')
        $this->deviceid = I('deviceid');

        $not_check_deviceid_action_array = array('query','index','listShare','addFav','delFav','listFav');
        $method = I('method');
        
        if( !(in_array(ACTION_NAME, $not_check_deviceid_action_array)||in_array($method, $not_check_deviceid_action_array))  && !$this->deviceid)
        {
            $this->out(null,1);
        }
    }

    public function keepalive()
    {
        echo 'uid:'.$this->uid.':keepalive';
    }

    public function add()
    {
    	$type = I('type');
    	//$name = I('name');
        $desc = I('desc');
        if(!$desc)
        {
            $this->out(null,1);
        }
        $result = $this->device->add($this->deviceid,$desc);
    	$this->outResult($result);
    }

    public function del()
    {
        $result = $this->device->del($this->deviceid);
        $this->outResult($result);
    }

    public function edit()
    {
        $desc = I('desc');
        if(!$desc)
        {
            $this->out(null,1);
        }
        $result = $this->device->edit($this->deviceid,$desc);
        $this->outResult($result);
    }

    public function query()
    {
        $type = I('type');

        $result = $this->device->query();
        $this->outResult($result);
    }

    public function share()
    {
        $result = $this->device->share($this->deviceid);
        $this->outResult($result);
    }

    public function unShare()
    {
        $result = $this->device->unShare($this->deviceid);
        $this->outResult($result);
    }

    public function listShare()
    {
        $start = I('start',-1,'int');
        $num = I('num',-1,'int');
        if($start < 0 || $num < 1)
        {
            $this->out(null,1);
        }
       
        $result = $this->device->listShare($start,$num);
        $this->outResult($result);
    }

    public function listFav()
    {
        // $start = I('start',-1,'int');
        // $num = I('num',-1,'int');
        // if($start < 0 || $num < 1)
        // {
        //     $this->out(null,1);
        // }
       
        $result = $this->device->listFav();
        $this->outResult($result);
    }

    public function pushAlarm()
    {
        $push = I('push',0,'intval');
       
        $result = $this->device->pushAlarm($this->deviceid, $push);
        $this->outResult($result);
    }

    public function addFav()
    {
        // $desc = I('desc');
        // $share = I('share',0,'int');
        $shareid = I('shareid');
        $uk = I('uk');

        $result = $this->device->addFav($shareid, $uk);
        $this->outResult($result);
    }

    public function delFav()
    {
        $shareid = I('shareid');
        $uk = I('uk');
        $result = $this->device->delFav($shareid,$uk);
        $this->outResult($result);
    }

    public function control()
    {
        $data = I('data');
        $result = $this->device->control($this->deviceid, $data);
        $this->outResult($result);
    }

    public function login()
    {
        
    }
    public function status() {

    }
}
