<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-6-4
 * Time: 下午5:19
 */

namespace Api\Controller;
use \Org\Uni\Device;

/**
 * 主要是针对APP客户的基础类
 */
class AppBase extends Base{
    protected $isLogin;
    protected $username;
    protected $uid;
    protected $token;
    protected $dao;

    protected $device;

    function __construct()
    {
        parent::__construct();

        $this->token = I('token');
        $domain = I('domain');

//        $clientType = I('clientType',-1,'int');
//        $clientVersion = I('clientVersion',-1,'int');
//        $clientOs = I('clientOs');
//        $ime = I('ime');
//        $md5 = I('md5');
//        $time = I('time',-1,'int');
//        $lang = I('lang');
//
//        if($clientType <=0 || $clientVersion <= 0 || !$clientOs || !$ime || $time <=0 || !$lang)
//        {
//            $this->out(null,3);
//        }
//
//        $md5_key = C('MD5_KEY');
//        $key = md5($md5_key.$ime.$time);
//        if($md5 != $key)
//        {
//            $this->out(null, 11);
//        }

        $API_NOT_CHECK_TOKEN_ACTION = C('API_NOT_CHECK_TOKEN_ACTION');
        $method = I('method');
        //请求有两种方式，一种是用ACTION_NAME 一种是用method参数
        $filter1 = CONTROLLER_NAME . '-' . ACTION_NAME;
        $filter2 = CONTROLLER_NAME . '-' . $method;

        //检查当前模块是否要进度用户认证
        if(!in_array($filter1, $API_NOT_CHECK_TOKEN_ACTION) && !in_array($filter2, $API_NOT_CHECK_TOKEN_ACTION))
        {
            if(!$this->token)
            {
                $this->out(null,3);
            }
            

            $this->uid = I('uid',0,'intval');
            $mod = M('User');
            $row = $mod->getByUid($this->uid);
            //var_dump($row);
            if(!$row || !is_array($row))
            {
                $this->username = $row['uname'];                
            }
            
            $this->isLogin = true;

//            $where = array();
//            $where['ime']=$ime;
//            $data = array();
//            $data['updatetime'] = time();
//            $mod = M('history');
//            $re = $mod->where($where)->data($data)->setInc('total')->save();
//            if(!$re)
//            {
//                $data['clientType'] = $clientType;
//                $data['clientVersion'] = $clientVersion;
//                $data['clientOs'] = $clientOs;
//                $data['ime'] = $ime;
//                $data['lang'] = $lang;
//                $data['token'] = $this->token;
//                $data['total'] = 0;
//                $mod->data($data)->add();
//            }
        }
        $this->device = new Device($this->uid,$this->token);
    }
} 