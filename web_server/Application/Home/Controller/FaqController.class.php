<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;

use Home\Controller;

class FaqController extends Base {

    function __construct()
    {
        parent::__construct();
       
    }
    public function index()
    {  
        $this->assign('FAQ_ON',' class="active"');
        $this->assign('TITLE','FAQ - '.$this->WEB_NAME);        
        $this->display();
    }    
}