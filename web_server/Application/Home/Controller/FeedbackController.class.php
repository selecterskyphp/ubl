<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;

use Home\Controller;

class FeedbackController extends Base {

    function __construct()
    {
        parent::__construct();
       
    }
    public function index()
    {  
        $this->assign('FEEDBACK_ON',' class="active"');
        $this->assign('TITLE','反馈 - '.$this->WEB_NAME);        
        $this->display();
    }    
    public function save()
    {
        $this->assign('jumpUrl',U('Index/index'));
        $this->success("操作成功");
    }
}