<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;

use Home\Controller;

class IndexController extends Base {

    private $device;
    function __construct()
    {
        parent::__construct();
       
    }
    public function index()
    {  
        $this->assign('HOME_ON',' class="active"');
        $this->assign('TITLE','首页 - '.$this->WEB_NAME);        
        $this->display();
    }    
}