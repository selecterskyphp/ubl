<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Common\Controller;

use Think\Controller;

class CommonBase extends Controller
{
    protected $WEB_NAME;
    protected $dao;

    function __construct()
    {
        parent::__construct();

        $this->WEB_NAME='HomeEye';

        $this->assign('__ACTION_NAME__',ACTION_NAME);
        $this->assign('__MOUDLE_NAME__',CONTROLLER_NAME);
        $this->assign('__THEME_NAME__',THEME_NAME);
        $this->assign('__TMPL__',THEME_PATH);

        //echo '__construct,'.ACTION_NAME.','.MOUDLE_NAME.','.THEME_NAME.','.THEME_PATH;
    }
} 
