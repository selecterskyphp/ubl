<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午3:10
 */
require_once 'curl_helper.php';

/**
 * 根据错误码返回一个错误信息
 * @param $code
 * @param int $type
 * @return mixed
 */
function errorByCode($code, $msg='', $type=0)
{
    $result = C('JSON_RESPONSE_MODEL');
    $result['code'] = $code;
    $result['errorType'] = $type;
    $result['message'] = $msg;
    if($code != 0 && !$msg)
    {
        $result['message'] = errorStringByCode($code, $type);
    }
    
    return $result;
}

function successByData($data)
{
    $result = C('JSON_RESPONSE_MODEL');
    $result['data'] = $data;
    return $result;
}

/**
 * 根据错误码返回具体的错误信息
 * @param $code
 * @return string
 */
function errorStringByCode($code, $type=0)
{
    $error = array();
    switch ($type) 
    {
        // case 0:
        //     $error = require(CONF_PATH.'error.php');
        //     break;
        case 1:
            $error = require(CONF_PATH.'baiduError.php');
            break;
        default:
            $error = require(CONF_PATH.'error.php');
            break;
    }
    $msg = 'unknown';
    if(is_array($error) && isset($error[$code]))
    {
        $msg = $error[$code];
    }
    return $msg;
}

function test()
{
    echo 'test';
}

function stringByDeviceStatus($status)
{
    if($status>0)
    {
        return "在线";
    }
    else
    {
        return "离线";
    }
}

/**
+----------------------------------------------------------
 * 产生随机字串，可用来自动生成密码
 * 默认长度6位 字母和数字混合 支持中文
+----------------------------------------------------------
 * @param string $len 长度
 * @param string $type 字串类型
 * 0 字母 1 数字 其它 混合
 * @param string $addChars 额外字符
+----------------------------------------------------------
 * @return string
+----------------------------------------------------------
 */
function rand_string($len = 6, $type = '', $addChars = '') {
    $str = '';
    switch ($type) {
        case 0 :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $addChars;
            break;
        case 1 :
            $chars = str_repeat ( '0123456789', 3 );
            break;
        case 2 :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . $addChars;
            break;
        case 3 :
            $chars = 'abcdefghijklmnopqrstuvwxyz' . $addChars;
            break;
        default :
            // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
            $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789' . $addChars;
            break;
    }
    if ($len > 10) { //位数过长重复字符串一定次数
        $chars = $type == 1 ? str_repeat ( $chars, $len ) : str_repeat ( $chars, 5 );
    }
    if ($type != 4) {
        $chars = str_shuffle ( $chars );
        $str = substr ( $chars, 0, $len );
    } else {
        // 中文随机字
        for($i = 0; $i < $len; $i ++) {
            $str .= msubstr ( $chars, floor ( mt_rand ( 0, mb_strlen ( $chars, 'utf-8' ) - 1 ) ), 1 );
        }
    }
    return $str;
}
