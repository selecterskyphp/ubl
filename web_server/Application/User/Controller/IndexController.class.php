<?php
// 本类由系统自动生成，仅供测试用途
namespace User\Controller;

use User\Controller;
use \Org\Uni\Device;
use \Org\Uni\BaiduApi;

class IndexController extends Base {

    private $device;
    function __construct()
    {
        parent::__construct();
        $this->dao = M('Device');
        $this->device = new Device($this->uid,$this->token);
    }
    public function test()
    {
        $this->display();
    }

    public function index(){
        //$this->redirect('Index/add', array('cate_id' => 2), 5, '页面跳转中...');
        $result = $this->device->query();
       //var_dump($result);
         if($result['0'] != 0)
        {
            $this->checkResult($result);
        }
        $list = $result['data']['list'];
       /* 
        for($i=0;$i<count($list);$i++)
        {
            if($list[$i]['thumbnail'])
            {
                $re = 0;
                $filename = $list[$i]['deviceid'].'.png';
                $path = RUNTIME_PATH . '/' . $filename;
                @$content = file_get_contents($list[$i]['thumbnail']);
                if($content)
                {
                    @$re = file_put_contents($path,$content);
                }
                if($re>0)
                {
                    $list[$i]['thumbnail'] = '/'.$path;
                }
                else
                {
                    $list[$i]['thumbnail'] = '';
                }

            }
        }*/
        //$list = $this->dao->where($where)->select();
        $this->assign('count',$result['data']['count']);
        $this->assign('list',$list);
        $this->assign('title','我的摄像机');
	    $this->display();
    }
    public function edit()
    {
        $code = I('get.code');
        $vo = array();
        if($code)
        {
            $this->assign('title','编辑摄像机');
            $where = array('uid'=>$this->uid,'code'=>$code);
            $vo = $this->dao->where($where)->find();
        }
        else
        {
            $this->assign('title','注册摄像机');
        }
        $this->assign('vo',$vo);
        $this->display();
    }
    public function save()
    {
        $code = I('post.code');
        $name = I('post.name');
        $id = I('post.id',0,'int');
        if($id>0)
        {
            $result = $this->device->edit($code,$name);
        }
        else
        {
            $result = $this->device->add($code,$name);

        }
        $this->checkResult($result);
    }

    public function play()
    {
        $code = I('get.code');
        $result = $this->device->startLive($code);
        $api = new BaiduApi();
        if($result['code'] !== 0)
        {
            $this->checkResult($result);
        }
        else
        {
            $this->assign('url',$result['data']['url']);
            $this->assign('ak',$api->getAK());
            $this->assign('sk', substr($api->getSK(),0,16));
            $this->assign('title',$code.' - 实况');
            $this->display();
        }

    }

    public function share()
    {
        $code = I('get.code');
        $value = I('get.value',-1);
        if($value == -1 || !$code)
        {
            $this->error('参数错误');
        }
        if($value>0)
        {
            $result = $this->device->unShare($code);
        }
        else
        {
            $result = $this->device->share($code);
        }
        $this->checkResult($result);
    }

    public function del()
    {
        $code = I('get.code');
        $result = $this->device->del($code);
        $this->checkResult($result);
    }


    public  function login()
    {
        $this->display();
    }

    public function doLogin()
    {
        $redirect_uri = C('USER_URL').U('Index/doLogin');
        $code = I('get.code','');
        $api = new BaiduApi();
        if(!$code)
        {
            $api->login($redirect_uri);
        }
        else
        {
            $result = $api->login($redirect_uri,$code);
            if($result['code'] !== 0)
            {
                $this->error($result['message'].',code:'.$result['code']);
            }
            else
            {
                $this->device->setToken($result['data']['access_token']);
                $result = $this->device->infoByToken($result['data']['refresh_token']);
                if($result['code'] !== 0)
                {
                    $this->error($result['message'].',code:'.$result['code']);
                }
                $uid = $result['data']['uid'];
                $uname = $result['data']['uname'];
                $token = $result['data']['token'];
              
                $this->token = $token;
                $this->username = $uname;
                $this->uid = $uid;
                    
                $this->setLoginStatus(true);
                $this->redirect("Index/index");
            }
        }
    }

    public function logout()
    {
        $this->setLoginStatus(false);
        $this->redirect('Index/login');
    }

    private function checkResult($result)
    {
        if($result['code'] !== 0)
        {
            $this->error($result['message'].',code:'.$result['code']);
        }
        else
        {
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('操作成功');
        }
    }
}
