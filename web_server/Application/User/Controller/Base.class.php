<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace User\Controller;


use Common\Controller\CommonBase;

class Base extends CommonBase
{

    protected $isLogin;
    protected $username;
    protected $uid;
    protected $token;
    protected $dao;

    function __construct()
    {
        parent::__construct();

        $this->username = I('session.username');
        $this->uid = I('session.uid');
        $this->token = I('session.token');

        if($this->token)
        {
            $this->isLogin = true;
        }
        else
        {
            $this->isLogin = false;
        }

        $notchecklogin = C('NOT_CHECK_LOGIN_ACTION');
        if(!in_array(ACTION_NAME,$notchecklogin))
        {
            if(!$this->isLogin)
            {
//                var_dump($_SESSION,$this->uid,$this->token,$this->username);
//                exit;
                $this->redirect('Index/login');
            }
        }

        $this->assign('__ACTION_NAME__',ACTION_NAME);
        $this->assign('__MOUDLE_NAME__',MOUDLE_NAME);
        $this->assign('__THEME_NAME__',THEME_NAME);
        $this->assign('__TMPL__',THEME_PATH);
        $this->assign('username',$this->username);
        $this->assign('uid',$this->uid);
        $this->assign('token',$this->token);
    }

    protected function setLoginStatus($status)
    {
        if($status)
        {
            $_SESSION['uid'] = $this->uid;
            $_SESSION['username'] = $this->username;
            $_SESSION['token']=$this->token;

            $this->isLogin = true;
        }
        else
        {
            $_SESSION['uid'] = '';
            $_SESSION['username'] = '';
            $_SESSION['token']='';

            $this->uid = '';
            $this->username = '';
            $this->token = '';
            $this->isLogin = false;
        }
    }


} 