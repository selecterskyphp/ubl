<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-30
 * Time: 下午2:18
 */

namespace Org\Uni;

use \Org\Uni;

class Device {

    private $baidu;
    private $dao;
    private $uid;
    private $token;
    function __construct($uid,$token)
    {
        $this->baidu = new BaiduApi();
        $this->baidu->setToken($token);
        $this->dao = M('Device');
        $this->uid = $uid;
        $this->token = $token;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    public function setToken($token)
    {
        $this->token = $token;
        $this->baidu->setToken($token);
    }

    public function infoByToken($refresh_token='')
    {
        $result = $this->baidu->userInfo();
        if($result['code'] != 0)
        {
            return $result;
        }
        $data = array(
            'updatetime'=>time(),
            'token'=>$this->token
        );
        if($refresh_token)
        {
            $data['refresh_token']=$refresh_token;
        }
    
        $resultData = $result['data'];
        $uid = $resultData['uid'];
        $uname = $resultData['uname'];
        $portrait = $resultData['portrait'];
        //给头像加入绝对地址
        if(strlen($portrait)>5)
        {
            $portrait = 'http://tb.himg.baidu.com/sys/portraitn/item/'.$resultData['portrait'];
        }
        $where = array('uid'=>$uid);
        $mod = M('User');
        $row = $mod->where($where)->find();
        if(!is_array($row))        
        {            
            $data['uid'] = $uid;
            $data['uname'] = $uname;
            $data['token'] = $this->token;
            $data['portrait'] = $portrait;
            $row['pushAlarm'] = 1;
            $data['pushAlarm'] = $row['pushAlarm'];
            
            $re = $mod->data($data)->add();
            if(!$re)
            {
//var_dump($mod->getLastSql());exit();
                return errorByCode(6);
            }
        }
        else
        {
            $data['uname'] = $uname;
            $mod->where($where)->data($data)->save();        
        }

        $data['version'] = C('API_VERSION');
        $data['token'] = $this->token;
        $data['uid'] = $uid;
        $data['uname'] = $uname; 
        $data['portrait'] = $portrait;
        $data['pushAlarm'] = $row['pushAlarm'];
        return successByData($data);
    }

    public function add($deviceid,$desc)
    {
        $where = array('uid'=>$this->uid,'deviceid'=>$deviceid);
        $row = $this->dao->where($where)->find();
        if(is_array($row))
        {
            //设备已经存在
            return errorByCode(4);
        }
        $result = $this->baidu->registerDevice($deviceid,$desc);
        if($result['code'] !== 0)
        {

            return $result;
        }
        else
        {
            $time = time();
            //六位数key
            $key = rand_string();
            
            $data = array(
                'uid'=>$this->uid,
                'deviceid'=>$deviceid,
                'desc'=>$desc,
                'streamid'=>$result['data']['stream_id'],
                'status'=>1,
                'share'=>0,
                'addtime'=>$time,                
                'key'=>$key,
                'romVer'=>'1.0',
                'pushAlarm'=>1
            );
            $re = $this->dao->data($data)->add();
             if(!$re)
            {
                //本地添加失败，尝试删除设备
                //var_dump($re);
                $this->baidu->delDevice($deviceid);
                return errorByCode(6);
            }
            $resultdata = $result['data'];
            $data = array(
                'deviceid'=>$resultdata['deviceid'],
                'requestid'=>$resultdata['request_id'],
                'streamid'=>$resultdata['stream_id'],
                'stream_id'=>$resultdata['stream_id'],
                'key'=>$code
            );
            $result = successByData($data);
            return $result;
        }
    }

    public function edit($deviceid,$desc)
    {
        $data = array('desc'=>$desc);
        $where = array('uid'=>$this->uid,'deviceid'=>$deviceid);
        $result = $this->baidu->updateDevice($deviceid,$desc);
        if($result['code'] !== 0)
        {
            return $result;
        }
        $re = $this->dao->where($where)->data($data)->save();
        //if(!$re)
        //{
        //    return errorByCode(8);
        //}
        $resultdata = $result['data'];
        $data = array(
            'deviceid'=>$resultdata['deviceid'],
            'requestid'=>$resultdata['request_id'],
            'desc'=>$resultdata['description'],
            'streamid'=>$resultdata['stream_id']);
        $result = successByData($data);
        return $result;
    }

    public function del($deviceid)
    {
        $where = array('uid'=>$this->uid,'deviceid'=>$deviceid);
        $result = $this->baidu->delDevice($deviceid);
        if($result['code'] !== 0)
        {
            return $result;
        }
        $re = $this->dao->where($where)->delete();
        if(!$re)
        {
            //return errorByCode(7);
        }
        $resultdata = $result['data'];
        $data = array(
            'deviceid'=>$deviceid,
        );
        $result = successByData($data);
        return $result;
    }

    public function query()
    {
        $result = $this->baidu->queryDevice();
        if($result['code'] != 0)
        {
            return $result;
        }
        $resultData = $result['data'];
        $data = array();
        $data['count'] = $resultData['count'];
        $data['requestid'] = $resultData['request_id'];

        $list = array();

        foreach($resultData['list'] as $v)
        {
            $deviceid = $v['deviceid'];
            if(!$deviceid || strlen($deviceid)<1)continue;
            $row = array();
            $row['desc'] = $v['description'];
            $row['share'] = $v['share'];
            $row['status'] = $v['status'];
            $row['streamid'] = $v['stream_id'];
            $row['cvrday'] = $v['cvr_day'];
            $row['expiretime'] = $v['expire_time'];
            $row['thumbnail'] = $v['thumbnail'];

            $where = array('uid'=>$this->uid);
            $where['deviceid'] = $deviceid;

            $r = $this->dao->getByDeviceid($deviceid);
            //记录不存在，则新增
            if(!$r)
            {
                $row['deviceid'] = $deviceid;
                $row['uid'] = $this->uid;
                $row['addtime'] = time();               
                $row['key'] = rand_string();;
                $row['romVer'] = '1.0';
                $r['pushAlarm'] = 1;
                $row['pushAlarm'] = $r['pushAlarm'];
                $re = $this->dao->data($row)->add();
            }
            else
            {
                //更新数据
                $this->dao->where($where)->data($row)->save();
            }
            $row['ip'] = $r['ip'];
            $row['pushAlarm'] = $r['pushAlarm'];
            $row['deviceid'] = $deviceid;
            $list[]=$row;
        }

        $data['list'] = $list;
        $result = successByData($data);
        return $result;
    }

    public function pushAlarm($deviceid,$push)
    {
        $where =array('deviceid'=>$deviceid);
        $data = array('pushAlarm'=>$push);
        $re = $this->dao->where($where)->data($data)->save();
        $result = successByData($data);
        return $result;
    }

    public function share($deviceid)
    {
        $where = array('uid'=>$this->uid,'deviceid'=>$deviceid);
        $result = $this->baidu->shareDevice($deviceid);
        return $result;
        /*
        if($result['code'] !== 0)
        {
            return $result;
        }

        $row = $this->dao->where($where)->field('share,desc')->find();
        if(!$row || !is_array($row))
        {
            //尝试取消共享
            $this->baidu->unShareDevice($deviceid);
            return errorByCode(5);
        }
        if($row['share'] != 1)
        {
            $data = array('share'=>1);
            $this->dao->where($where)->data($data)->save();
        }

        $resultData = $result['data'];
        $mod = M('Share');
        $data = array();
        $data['deviceid'] = $deviceid;
        $data['desc'] = $row['desc'];
        $data['uid'] = $this->uid;
        $data['type'] = 2;
        $data['shareid'] = $resultData['shareid'];
        $data['uk'] = $resultData['uk'];
        $data['password'] = $resultData['password'];
        $re = $mod->data($data)->add();
        if(!$re)
        {
            //尝试取消共享
            $this->baidu->unShareDevice($deviceid);
            return errorByCode(6);
        }
        unset($data['deviceid'],$data['desc'],$data['uid'],$data['type']);
        $reslt = successByData($data);
        return $result;*/
    }

    public function unShare($deviceid)
    {
        $where = array('uid'=>$this->uid,'deviceid'=>$deviceid);
        $result = $this->baidu->unShareDevice($deviceid);
        return $result;
        /*
        if($result['code'] !== 0)
        {
            return $result;
        }
        $data = array('share'=>0);
        $this->dao->where($where)->data($data)->save();

        $mod = M('Share');
        $mod->where($where)->delete();

        $data = array('requestid'=>$result['data']['request_id']);
        $result = successByData($data);
        return $result;*/
    }

    private function listShareByDB($start,$limit)
    {
        $mod = M('Share');
        $where = array('uid'=>$this->uid);
        $count = $mod->where($where)->count();
        $list = array();
        if($count>0)
        {
            $list = $mod->where($where)->limit($start,$limit)->select();
        }
        $data = array();
        $data['count'] = $count;
        $data['list'] = $list;
        $result = successByData($data);

        return $result;
    }

    private function listShareByBaidu($start,$limit)
    {
        $result = $this->baidu->queryShareDevice($start,$limit);
        if($result['code'] != 0)
        {
            return $result;
        }
        $resultData = $result['data'];

        $data = array();
        $data['count'] = $resultData['count'];
        $data['requestid'] = $resultData['request_id'];
        $list = array();
        $mod = M('Fav');
        foreach($resultData['device_list'] as $v)
        {
            $row = array();
            $row['desc'] = $v['description'];
            $row['status'] = $v['status'];
            $row['share'] = $v['share'];
            $row['thumbnail'] = $v['thumbnail'];
            $row['shareid'] = $v['shareid'];
            $row['uk'] = $v['uk'];
            $row['deviceid'] = $v['deviceid'];
            $row['isFav'] = 0;
            //用户已经登录，查询收藏状态
            if($this->uid)
            {
                $where = array('shareid'=>$v['shareid'], 'uk'=>$v['uk'],'uid'=>$this->uid);
                $r = $mod->where($where)->find();
                if($r)
                {
                    $row['isFav'] = 1;
                }
            }            
            
            $list[] = $row;
        }
        $data['list'] = $list;
        $result = successByData($data);
        return $result;
    }

    public function listShare($start,$limit)
    {
        return $this->listShareByBaidu($start,$limit);
    }

    public function addFav($shareid, $uk)
    {
        $result = $this->baidu->subscribe($shareid, $uk);
        //出错了，直接返回
        if($result['code'] != 0)
        {
            return $result;
        }
        //如果没有入库，则入库
        $mod = M('Fav');
        $where = array('shareid'=>$shareid,'uk'=>$uk,'uid'=>$this->uid);
        $row = $mod->where($where)->find();
        if(!$row)
        {
            $data = array('shareid'=>$shareid,'uk'=>$uk,'uid'=>$this->uid);
            $res = $mod->data($data)->add();
            if(!$res)
            {
                //尝试回删收藏
                $this->delFav($shareid, $uk);
                return errorByCode(6);
            }
        }
        return $result;
    }

    public function delFav($shareid, $uk)
    {
        $result = $this->baidu->unSubscribe($shareid, $uk);
        //var_dump($shareid,$uk,$result);
        //出错了，直接返回
        if($result['code'] != 0)
        {
            return $result;
        }
        //删除数据库里面的记录
        $mod = M('Fav');
        $where = array('shareid'=>$shareid,'uk'=>$uk,'uid'=>$this->uid);
        $mod->where($where)->delete();
        return $result;
    }

    public function listFav()
    {
        $result = $this->baidu->querySubscribe();
        if($result['code'] != 0)
        {
            return $result;
        }
        $resultData = $result['data'];
        $data = array();
        $data['count'] = $resultData['count'];
        $data['requestid'] = $resultData['request_id'];
        $list = array();
        foreach($resultData['device_list'] as $v)
        {
            $row = array();
            $row['shareid'] = $v['shareid'];
            $row['uk'] = $v['uk'];
            $row['desc'] = $v['description'];
            $row['share'] = $v['share'];
            $row['thumbnail'] = $v['thumbnail'];
            $row['deviceid'] = $v['deviceid'];
            $row['status'] = $v['status'];
            $list[] = $row;
        }
        $data['list'] = $list;
        $result = successByData($data);
        return $result;
    }

    // public function listFav($start, $limit)
    // {
    //     $mod = M('Fav');
    //     $where = array('uid'=>$this->uid);
    //     $count = $mod->where($where)->count();
    //     $list = array();
    //     if($count>0)
    //     {
    //         $list = $mod->where($where)->order('listorder desc,id desc')->limit($start,$limit)->select();
    //     }
    //     $data = array();
    //     $data['count'] = $count;
    //     $data['list'] = $list;
    //     $result = successByData($data);

    //     return $result;
    // }

    // public function addFav($deviceid, $desc, $share=0, $shareid='', $uk='')
    // {
    //     $mod = M('Fav');
    //     $where = array('uid'=>$this->uid, 'deviceid'=>$deviceid);
    //     $data = $mod->where($where)->find();
    //     if(is_array($data))
    //     {
    //         $result = errorByCode(13);
    //         return $result;
    //     }

    //     $data = array('uid'=>$uid, 
    //         'deviceid'=>$deviceid, 
    //         'desc'=>$desc, 
    //         'share'=>$share, 
    //         'shareid'=>$shareid,
    //         'uk'=>$uk,
    //         'listorder'=>0
    //         );
    //     $re = $mod->data($data)->add();
    //     if(!$re)
    //     {
    //         return errorByCode(6);
    //     }

    //     $result = successByData($re);
    //     return $result;
    // }

    // public function delFav($deviceid)
    // {
    //     $mod = M('Fav');
    //     $where = array('uid'=>$this->uid, 'deviceid'=>$deviceid);
    //     $re = $mod->where($where)->delete();
    //     if($re)
    //     {
    //         $result = successByData($re);
    //         return $result;
    //     }
    //     else
    //     {
    //         return errorByCode(7);
    //     }
    // }

    public function startLive($deviceid)
    {
        $result = $this->baidu->startLive($deviceid);
        // var_dump($result);
        // exit;
        if($result['code'] != 0)
        {
            return $result;
        }
        $resultdata = $result['data'];
        $data = array('requestid'=>$resultdata['request_id'],'url'=>$resultdata['url']);
        $result = successByData($data);
        return $result;
    }

    public function startLiveByShare($shareid, $uk)
    {
        $result = $this->baidu->startLiveByShare($shareid, $uk);
        if($result['code'] != 0)
        {
            return $result;
        }
        $resultdata = $result['data'];
        $data = array('requestid'=>$resultdata['request_id'],'url'=>$resultdata['url']);
        $result = successByData($data);
        return $result;
    }

    public function queryReplay($deviceid,$startTime,$endTime)
    {
        $result = $this->baidu->queryReplay($deviceid,$startTime,$endTime);
        if($result['code'] != 0)
        {
            return $result;
        }
        $resultdata = $result['data'];

        $data = array();
        $data['count'] = count($resultdata['results']);
        $data['requestid'] = $resultdata['request_id'];
        $data['streamid'] = $resultdata['stream_id'];
        $list = array();
        foreach($resultdata['results'] as $v)
        {
            $row = array();
            $row['st'] = $v[0];
            $row['et'] = $v[1];
            $list[]=$row;
        }
        $data['list'] = $list;
        $result = successByData($data);
        return $result;
    }

    public function startReplay($deviceid,$starttime,$endTime)
    {
        $url = $this->baidu->uriForReplay($deviceid,$starttime,$endTime);
        $data = array('url'=>$url);
        $result = successByData($data);
        return $result;
    }

    public function queryLastThumbail($deviceid)
    {
        $url = $this->baidu->queryLastThumbail($deviceid);
        $data = array('url'=>$url);
        $result = successByData($data);
        return $result;
    }

    public function control($deviceid,$data)
    {
        $result = $this->baidu->control($deviceid,$data);
        return $result;
    }

}
